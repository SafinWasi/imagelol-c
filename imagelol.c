#include <stdio.h>
#include <math.h>
#include <stdint.h>
#include <limits.h>
#include <unistd.h>

#define STB_IMAGE_IMPLEMENTATION
#include "include/stb_image.h"
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "include/stb_image_write.h"

#define u64 uint64_t
#define byte unsigned char
#define bool char

enum channels {
	r=0, 
    g=1, 
    b=2
};

typedef enum channels channels;


byte sample_image(byte* image, int width, int height, double x, double y, channels channel) {
    return image[((size_t) (y * (double)(height-1)) * 
        (size_t)width + (size_t)(x*(double)(width-1))) * 
        3 + channel];
}

void write_bits(byte* image, u64 *current_pos, byte *current_bit, byte bits, int bit_n, byte bit_mask) {
    u64 pos = *current_pos;
    u64 bit = *current_bit;
    for(byte i = 0; i < 8; i++) {
		if(bit == 0) {
			image[pos] &= bit_mask;
		}
		image[pos] |= ((bits >> (8-i-1))&1) << (bit_n-bit-1); 
		if(bit >= bit_n - 1) {
			bit = 0;
			pos++;
            *current_bit = bit;
            *current_pos = pos;
		} else {
            bit++;
			*current_bit = bit;
		}
	}
}

byte read_bit(byte* image, u64 *current_pos, byte *current_bit, int bit_n) {
    u64 pos = *current_pos;
    u64 bit = *current_bit;
	byte ret = (image[bit] >> (bit_n-bit-1)) & 1;
	if(bit >= bit_n - 1){
        pos++;
        bit = 0;
		*current_pos = pos;
		*current_bit = bit;
	}else{
        bit++;
		*current_bit = bit;
	}
	return ret;
}

int main(int argc, char **argv) {
    int arg_count = 0;
    int bit_n = 3;
    bool embed = 0;
    bool bad_usage = 0;
    bool extract = 0;
    FILE *infile;
    FILE *inimage;
    FILE *outimage;
    if(argc == 2 && argv[1][0] != '-') {
        printf("Extract image\n");
    } else if ((argc == 3) && (argv[1][0] != '-') && (argv[2][0] != '-')) {
        printf("First to second\n");
    } else {
        printf("Something else\n");
    }
}