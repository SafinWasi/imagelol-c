CC=gcc
CFLAGS = -g -Wall -lm
all: imagelol.c
	$(CC) $(CFLAGS) -o imagelol-c imagelol.c -lm

clean:
	$(RM) imagelol-c